function combinatorics(input) {
  let width = input.length,
    height = 1, //init height of combinations
    combsArr = [];
  for (let i = 0; i < width; i++) {
    height *= input[i].length;
  }
  for (let i = width - 1; i > -1; i--) {
    let lengthsI = i,
      lengthsAmount = 1;
    while (lengthsI) {
      lengthsAmount *= input[lengthsI - 1].length;
      lengthsI--;
    }
    let tmpLengthsAmount = lengthsAmount;
    for (let j = 0, m = 0; j < height; j++) {
      if (i === width - 1) {
        combsArr.push([]);//init with empty arrays
      }
      if (!tmpLengthsAmount) {
        m++;
        if (m === input[i].length) {
          m = 0;
        }
        tmpLengthsAmount = lengthsAmount;
      }
      tmpLengthsAmount--;
      combsArr[j][i] = input[i][m];
    }
  }
  return combsArr;
}

module.exports.combinatorics = combinatorics;

module.exports.test = function() {
  const data = [['A1', 'B1'], ['A2', 'B2', 'C2'], ['A3', 'B3', 'C3']];
  const result = combinatorics(data);
  console.log(result);
};

module.exports.test();